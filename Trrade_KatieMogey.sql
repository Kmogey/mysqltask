--Find all trades by a given trader on a given stock
select case when t.buy = 3 then 'Buy' else 'Sell' end,t.id, t.price, t.instant
from trade t
join position p on p.opening_trade_id = t.id or 
p.closing_trade_id = t.id
where p.trader_id = 1 and t.stock = 'mrk'
order by t.instant asc;



--Find the total profit or loss for a given trader over the day, as the sum of the product of trade size and price for all sales, minus the sum of the product of size and price for all buys.

select sum(t.size * t.price * (case when t.buy = 1 then -1 else 1 end))
from trade t
  join position p on p.opening_trade_id = t.id or p.closing_trade_id = t.id
where p.trader_id = 1
  and t.stock = 'mrk'
  and p.closing_trade_id != p.opening_trade_id
  and p.closing_trade_id is not null;


