--Show all information about the bond with the CUSIP '28717RH95'.
select * from bond where  CUSIP ='28717RH95';

--Show all information about all bonds, in order from shortest (earliest maturity) to longest (latest maturity).
select * from bond 
order by maturity asc

--Calculate the value of this bond portfolio, as the sum of the product of each bond's quantity and price.
select SUM(quantity), sum(price)
from bond;

--Show the annual return for each bond
 Select coupon/ 100* quantity
  from bond;

  --Show bonds only of a certain quality and above
  Select id, rating 
  from bond where rating = 'AA2';

  --Show the average price and coupon rate for all bonds of each bond rating.
 -- select avg(price), avg(coupon), rating
 -- from bond
 -- group by rating;
  select avg(price), avg(coupon), bond.rating
  from bond join rating on 
  bond.rating = rating.rating group by bond.rating;

  --Calculate the yield for each bond, as the ratio of coupon to price. 
  
  select (coupon/price) , r.expected_yield
  from bond b 
  join rating r on r.rating = b.rating
  where (coupon/price) <r.expected_yield;

 